const array = [1,2,3,"A"];

array.forEach((value, index) => console.warn("value", value, index));

const mappedArray = array.map((value) => value + 1)
console.warn("mappedArray", mappedArray)

const reducedArray = array.reduce((acc, cur) => acc + cur, 0)
console.warn("reducedArray", reducedArray)

const arrayOfObjects = [{age: 1, team: 'red'},{age: 2, team: 'red'},{age: 3,team: 'blue'}]
const reducedArrayOfObjects = arrayOfObjects.reduce((acc, cur) => acc + cur.age, 0)
console.warn("reducedArrayOfObjects", reducedArrayOfObjects)


const transformedArray = arrayOfObjects.reduce((acc, cur) => {
    if (acc[cur.team]) {
        acc[cur.team] = acc[cur.team] + cur.age;
    } else {
        acc[cur.team] = cur.age;
    }
    return acc
}, {})
console.warn("transformed", transformedArray)

const filteredArray = array.filter((value) => value < 3)
console.warn("filteredArray", filteredArray)

const findArray = array.find((value) => value === 1)
console.warn("findArray", findArray)

const findArrayOfObjects = arrayOfObjects.find((value) => value.age === 1)
console.warn("findArrayOfObjects", findArrayOfObjects)