const sum = (x,y) => {
    return x + y
}

const diff = (x,y) => {
    return x - y
}

const mul = (x,y) => {
    return x * y
}

const div = (x,y) => {
    return x / y
}

module.exports = {
    sum,
    diff,
    mul,
    div
}