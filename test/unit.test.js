const assert = require('assert')
const { sum, diff, mul, div } = require('../src/sum')

describe('Summation function', () => {
    it('should add 1 and 2 to equal 3', () => {
        assert.equal(sum(1,2), 3)
    })

    it('should add 1 and 2 to not equal to 4', () => {
        assert.notEqual(sum(1,2), 4)
    })

    it('should add 1 and null to equal to 1', () => {
        assert.equal(sum(1, null), 1)
    })

    it('should add 1 and A to equal to 1A', () => {
        assert.equal(sum(1, 'A'), '1A')
    })
})

describe('Difference function', () => {
    it('should subtract 1 and 2 to equal -1', () => {
        assert.equal(diff(1,2), -1)
    })

    it('should subtract 1 and 2 to not equal to 4', () => {
        assert.notEqual(diff(1,2), 4)
    })

    it('should subtract 1 and null to equal to 1', () => {
        assert.equal(diff(1, null), 1)
    })

    it('should subtract 1 and A to not equal to 1A', () => {
        assert.notEqual(diff(1, 'A'), '1A')
    })
})

describe('Multiplication function', () => {
    it('should multiply 1 and 2 to equal 2', () => {
        assert.equal(mul(1,2), 2)
    })

    it('should multiply 1 and 2 to not equal to 4', () => {
        assert.notEqual(diff(1,2), 4)
    })

    it('should multiply 1 and null to equal to 1', () => {
        assert.equal(diff(1, null), 1)
    })

    it('should multiply 1 and A to not equal to 1A', () => {
        assert.notEqual(diff(1, 'A'), '1A')
    })
})

describe('Division function', () => {
    it('should divide 1 and 2 to equal 1/2', () => {
        assert.equal(div(1,2), 0.5)
    })

    it('should divide 1 and 2 to not equal to 4', () => {
        assert.notEqual(div(1,2), 4)
    })

    it('should divide 1 and null to equal to 1', () => {
        assert.equal(div(1, null), Infinity)
    })

    it('should divide 1 and A to not equal to 1A', () => {
        assert.notEqual(div(1, 'A'), '1A')
    })
})
